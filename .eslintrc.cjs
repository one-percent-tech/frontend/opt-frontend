module.exports = {
  root: true,
  env: { browser: true, es2020: true, jest: true },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:jsx-a11y/recommended',
    'plugin:promise/recommended',
    'plugin:unicorn/recommended',
    'prettier',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: './tsconfig.json',
  },
  plugins: ['import', 'jsx-a11y', 'prettier', 'promise', 'unicorn'],
  settings: {
    'import/resolver': {
      alias: [['/', '@src', '@components', '@layouts', '@stores']],
    },
  },
  globals: {
    document: true,
    window: true,
  },
  rules: {
    // Allows promise rejections without error objects
    'prefer-promise-reject-errors': 'off',

    // Allows the use of any file extension
    'import/no-unresolved': 'off',

    // Warns if quotes are not single or if escape is not avoided
    quotes: ['warn', 'single', { avoidEscape: true }],

    // in plain CommonJS modules, you can't use `import foo = require('foo')` to pass this rule, so it has to be disabled
    '@typescript-eslint/no-var-requires': 'off',

    // this rule is not compatible with TypeScript's `noImplicitAny` option
    '@typescript-eslint/no-explicit-any': 'off',

    // Enforces the use of Prettier for code formatting
    'prettier/prettier': [
      'error',
      {
        printWidth: 100,
        tabWidth: 2,
        useTabs: false,
        semi: true,
        singleQuote: true,
        proseWrap: 'preserve',
        arrowParens: 'always',
        bracketSpacing: true,
        endOfLine: 'lf',
        eslintIntegration: false,
        htmlWhitespaceSensitivity: 'ignore',
        ignorePath: '.prettierignore',
        jsxBracketSameLine: false,
        jsxSingleQuote: false,
        parser: 'babel-ts',
        requireConfig: false,
        stylelintIntegration: false,
        trailingComma: 'all',
        tslintIntegration: false,
      },
      { usePrettierrc: false },
    ],

    // Arrow patterns
    'arrow-parens': ['error', 'always'],

    // Enforces a maximum cyclomatic complexity of 5
    complexity: ['error', { max: 5 }],

    // Enforces that default parameters should be last
    'default-param-last': 'error',

    // this rule solve ESLint and Prettier conflict of switch/case indentation
    indent: ['error', 2, { SwitchCase: 1 }],

    // unicorn filename-case
    'unicorn/filename-case': [
      'error',
      {
        cases: {
          // "kebabCase": true,
          // "snakeCase": true,
          camelCase: true,
          pascalCase: true,
          // ignore: ['^FOOBAR\\.tsx$', '^(B|b)az', '\\.SOMETHING\\.tsx$', /^vendor/i],
          // "multipleFileExtensions": false
        },
      },
    ],

    // unicorn/prevent-abbreviations
    'unicorn/prevent-abbreviations': [
      'error',
      {
        replacements: {
          args: false,
          params: false,
          props: false,
        },
      },
    ],

    // Allows defined but never used variables
    'no-unused-vars': 'off',

    // Not Allows var, use let or const
    'no-var': 'error',

    // Allows plusplus
    'no-plusplus': 'off',

    // Allows catch empty
    'no-empty': ['error', { allowEmptyCatch: true }],

    // Allows the use of debugger during development only
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',

    // Allows the use of console during development only
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
  },
  overrides: [
    {
      extends: [
        'airbnb',
        'airbnb-typescript/base',
        'airbnb/hooks',
        'plugin:react-hooks/recommended',
      ],
      // Define the configuration for `.astro` file.
      files: ['*.ts', '*.tsx', '*.mts', '*.d.ts'],
      // Allows Astro components to be parsed.
      parser: '@typescript-eslint/parser',
      // Parse the script in `.astro` as TypeScript by adding the following configuration.
      // It's the setting you need when using TypeScript.
      plugins: ['react-refresh'],
      parserOptions: {
        project: './tsconfig.json',
        extraFileExtensions: ['.ts', '.tsx', '.mts', '.d.ts'],
      },
      rules: {
        // Warns if components are not exported for react-refresh
        'react-refresh/only-export-components': ['warn', { allowConstantExport: true }],
        // Starting with React version 17.0, a new JSX transform was introduced, and it’s no longer necessary to import React.
        'react/react-in-jsx-scope': 'off',
        // Enforces the file extension for JSX files
        'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx', '.ts', '.tsx'] }],
      },
    },
    {
      // Define the configuration for `.astro` file.
      extends: [
        'plugin:astro/recommended',
        'plugin:astro/jsx-a11y-recommended',
      ],
      files: ['*.astro'],
      // Allows Astro components to be parsed.
      parser: 'astro-eslint-parser',
      // Parse the script in `.astro` as TypeScript by adding the following configuration.
      // It's the setting you need when using TypeScript.
      parserOptions: {
        // parser: '@typescript-eslint/parser',
        parser: 'astro-eslint-parser',
        project: './tsconfig.astro.json',
        extraFileExtensions: ['.astro'],
      },
      rules: {
        'prettier/prettier': 'off',
      },
    },
    {
      extends: ['plugin:vue/vue3-recommended'],
      // Define the configuration for `.astro` file.
      files: ['*.vue'],
      // Allows Astro components to be parsed.
      parser: 'vue-eslint-parser',
      // Parse the script in `.astro` as TypeScript by adding the following configuration.
      // It's the setting you need when using TypeScript.
      parserOptions: {
        parser: '@typescript-eslint/parser',
        extraFileExtensions: ['.vue'],
        project: './tsconfig.vue.json',
        sourceType: 'module',
        ecmaFeatures: {
          jsx: true,
        },
      },
      rules: {
        'prettier/prettier': 'off',
        'vue/multiline-html-element-content-newline': 'off'
      },
    },
    // ...
  ],
};
