import { defineConfig } from 'astro/config';
import react from '@astrojs/react';
import vue from '@astrojs/vue';

// https://astro.build/config
export default defineConfig({
  base: '/',
  server: {
    port: 5003,
    strictPort: true,
    host: true,
  },
  // Enable React to support React JSX components.
  integrations: [react(), vue({ appEntrypoint: '/src/_vueApp', devtools: false })],
  devToolbar: {
    enabled: true,
  },
});
