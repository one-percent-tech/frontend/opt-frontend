import { atom } from 'nanostores';

const counterStore = atom({ count: 1 });
const increaseCounter = () => counterStore.set({ count: counterStore.get().count + 1 });
const decreaseCounter = () => {
  counterStore.set({ count: counterStore.get().count - 1 });
};

export { counterStore, increaseCounter, decreaseCounter };
