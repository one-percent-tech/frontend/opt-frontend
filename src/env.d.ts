/* eslint-disable unicorn/prevent-abbreviations */

/// <reference types="astro/client" />

declare namespace NodeJS {
  interface ProcessEnv {
    NODE_ENV: string;
    ROUTER_MODE: 'hash' | 'history' | 'abstract' | undefined;
    ROUTER_BASE: string | undefined;
  }
}
