import type { App } from 'vue';

import NaiveUI from 'naive-ui';
import { devtools } from '@nanostores/vue/devtools';
import { counterStore } from './stores/example/navigate_with_store/counter';

const vueApp = (app: App) => {
  app.use(NaiveUI);
  app.use(devtools, { counterStore });
};
export default vueApp;
