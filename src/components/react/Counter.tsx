import { useState } from 'react';
import './Counter.css';

export default function Counter({
  children,
  count: initialCount,
}: {
  children: JSX.Element;
  count: number;
}) {
  const [count, setCount] = useState(initialCount);
  const add = () => setCount((index) => index + 1);
  const subtract = () => setCount((index) => index - 1);

  return (
    <>
      <div className="counter">
        <div className="inline-flex">
          <button type="button" onClick={subtract}>
            -
          </button>
          <pre>{count}</pre>
          <button type="button" onClick={add}>
            +
          </button>
        </div>
      </div>
      <div className="counter-message">{children}</div>
    </>
  );
}
