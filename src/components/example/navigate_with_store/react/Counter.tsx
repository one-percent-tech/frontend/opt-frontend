import './Counter.css';
import { navigate } from 'astro:transitions/client';
import { useStore } from '@nanostores/react';
import {
  counterStore,
  increaseCounter,
  decreaseCounter,
} from '@stores/example/navigate_with_store/counter';

const nv = () => {
  navigate('/example/navigate_with_store/vue');
};

export default function Counter() {
  const counter = useStore(counterStore);

  return (
    <div className="counter">
      <button type="button" onClick={nv}>
        To Vue
      </button>
      <br />
      <button type="button" onClick={decreaseCounter}>
        -
      </button>
      <div>{counter.count}</div>
      <button type="button" onClick={increaseCounter}>
        +
      </button>
    </div>
  );
}
